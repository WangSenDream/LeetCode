class Solution:
    def longestCommonPrefix(self, strs: list) -> str:
        '''
        :type strs: List[str]
        :rtype: str
        '''
        len = strs.__len__()

        if len == 0:
            return ''

        if len == 1:
            return strs[0]

        pre = ''
        i2 = 0

        while True:

            tmp = ''

            for i1 in range(0, len):
                if i2 == strs[i1].__len__():
                    return pre

                if i1 == 0:
                    tmp = strs[i1][i2]
                else:
                    if tmp != strs[i1][i2]:
                        return pre

                    if i1 + 1 == len:
                        pre += tmp

            i2 += 1


if __name__ == '__main__':
    strs = ['flower', 'flow', 'flight']
    print(Solution().longestCommonPrefix(strs))
