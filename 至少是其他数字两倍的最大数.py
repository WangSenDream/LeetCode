class Solution:
    def dominantIndex(self, nums: list) -> int:
        """
        :type nums: List[int]
        :rtype: int
        """
        len = nums.__len__()

        a = 0
        b = 0
        c = 0

        for i in range(0, len):
            if nums[i] > b:
                if nums[i] > a:
                    b = a
                    a = nums[i]
                    c = i
                    continue

                b = nums[i]

        if a // 2 >= b:
            return c

        return -1


if __name__ == '__main__':
    nums = [0, 0, 2, 3]
    print(Solution().dominantIndex(nums))
