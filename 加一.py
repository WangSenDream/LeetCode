class Solution:
    def plusOne(self, digits: list) -> list:
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        len = digits.__len__()

        i = 1

        digits[len - i] += 1

        while digits[len - i] == 10:
            digits[len - i] = 0

            if len == i:
                digits.insert(0, 1)
            else:
                digits[len - i - 1] += 1

            i += 1

        return digits


if __name__ == '__main__':
    digits = [9, 9]
    print(Solution().plusOne(digits))
