class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        hl = haystack.__len__()
        nl = needle.__len__()

        if nl == 0:
            return 0

        p = -1

        for h in range(0, hl):
            if haystack[h] == needle[0] and p == -1:
                p = h

                if hl - h < nl:
                    return -1

                for n in range(1, nl):
                    h += 1

                    if haystack[h] != needle[n]:
                        p = -1
                        break

            if p != -1:
                break

        return p


if __name__ == '__main__':
    haystack = 'mississippi'
    needle = 'issip'
    print(Solution().strStr(haystack, needle))
