class Solution:
    def reverseString(self, s: str) -> str:
        """
        :type s: str
        :rtype: str
        """
        # l = list(s)
        # len = l.__len__()
        #
        # i = 0
        # j = len - 1
        #
        # while i < j:
        #     tmp = l[i]
        #     l[i] = l[j]
        #     l[j] = tmp
        #     i += 1
        #     j -= 1
        #
        # return ''.join(l)

        # 作弊的太多了 我能怎么办
        return s[::-1]


if __name__ == '__main__':
    s = '123456'
    print(Solution().reverseString(s))
