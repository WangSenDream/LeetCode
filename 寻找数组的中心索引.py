class Solution:
    def pivotIndex(self, nums: list) -> int:
        """
        :type nums: List[int]
        :rtype: int
        """
        len = nums.__len__()

        count = 0
        for num in nums:
            count += num

        left = 0
        for i in range(0, len):
            if left + left + nums[i] == count:
                return i

            left += nums[i]

        return -1


if __name__ == '__main__':
    nums = [-1, -1, 0, 1, 1, 0]
    print(Solution().pivotIndex(nums))
